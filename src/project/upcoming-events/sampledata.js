{
    "StartIndex": 0,
    "Count": 10,
    "NextPageUri": "https://api.arlo.co/theme-test/api/2012-02-01/pub/resources/eventsearch/?format=json&top=10&fields=Name,StartDateTime,EndDateTime,ViewUri,Summary,TimeZone,RegistrationInfo&skip=10",
    "Items": [{
        "ViewUri": "http://theme-test.learningsourceapp.com/courses/54-building-a-marketing-machine-for-your-accounting-firm",
        "RegistrationInfo": {
            "RegisterUri": "http://theme-test.learningsourceapp.com/register?sgid=f3713880b0754d3db5d23f35d90a6e87",
            "RegisterMessage": "Register"
        },
        "Name": "Building a Marketing Machine for Your Accounting Firm",
        "Summary": "Module 7 of the Modern Marketing Academy is the second of 4 modules on how to sell your firm's additional value-add services. The first 5 modules in The Academy address marketing. In module 10 we show you how to objectively measure and monitor your firm's marketing and selling effectiveness.",
        "StartDateTime": "2015-09-15T08:00:00.0000000+12:00",
        "EndDateTime": "2015-09-15T10:00:00.0000000+12:00",
        "TimeZone": "NZST"
    }, {
        "ViewUri": "http://theme-test.learningsourceapp.com/courses/27-business-skills-for-new-managers",
        "RegistrationInfo": {
            "RegisterUri": "http://theme-test.learningsourceapp.com/register?sgid=8ea9c483118f4270af013da25541b605",
            "RegisterMessage": "Register"
        },
        "Name": "Business Skills for New Managers",
        "Summary": "Most strategic planning processes work from today forward not from the future back - implicitly assuming, whatever the evidence to the contrary, that the future will be more or less like the present.",
        "StartDateTime": "2015-10-09T08:00:00.0000000+13:00",
        "EndDateTime": "2015-10-09T12:00:00.0000000+13:00",
        "TimeZone": "NZDT"
    }, {
        "ViewUri": "http://theme-test.learningsourceapp.com/courses/27-business-skills-for-new-managers",
        "RegistrationInfo": {
            "RegisterUri": "http://theme-test.learningsourceapp.com/register?sgid=885ec61a9f434ea39daaf748fc916792",
            "RegisterMessage": "Register"
        },
        "Name": "Business Skills for New Managers",
        "Summary": "Most strategic planning processes work from today forward not from the future back - implicitly assuming, whatever the evidence to the contrary, that the future will be more or less like the present.",
        "StartDateTime": "2015-10-09T08:00:00.0000000+13:00",
        "EndDateTime": "2015-10-09T12:00:00.0000000+13:00",
        "TimeZone": "NZDT"
    }, {
        "ViewUri": "http://theme-test.learningsourceapp.com/courses/43-creative-thinking-ideas-that-ignite-your-business",
        "RegistrationInfo": {
            "RegisterUri": "http://theme-test.learningsourceapp.com/register?sgid=9680be0f0bf0483b96370c01bd3ebf71",
            "RegisterMessage": "Register"
        },
        "Name": "Creative Thinking: Ideas that Ignite Your Business",
        "Summary": "This Short Course is aimed directly at enabling you or your organisation to understand and practise the implementation of process mapping. The course will provide you with the tools and processes and a proven methodology for identifying your current AS IS business process and enable you to develop the SHOULD BE roadmap",
        "StartDateTime": "2015-10-09T08:00:00.0000000+13:00",
        "EndDateTime": "2015-10-09T12:00:00.0000000+13:00",
        "TimeZone": "NZDT"
    }, {
        "ViewUri": "http://theme-test.learningsourceapp.com/courses/49-dangerous-good-handling-renewal",
        "RegistrationInfo": {
            "RegisterUri": "http://theme-test.learningsourceapp.com/register?sgid=733cac8d10ea41dd9553532e605cd952",
            "RegisterMessage": "Register"
        },
        "Name": "Dangerous Good Handling Renewal",
        "Summary": "The AUMI Certificate in Management is recommended as a qualification suitable for people who are newly appointed into a management role, who are planning to move into a management role, or are looking to get an introductory management qualification.",
        "StartDateTime": "2015-10-09T08:00:00.0000000+13:00",
        "EndDateTime": "2015-10-09T12:00:00.0000000+13:00",
        "TimeZone": "NZDT"
    }, {
        "ViewUri": "http://theme-test.learningsourceapp.com/courses/19-developing-strengths-into-talents",
        "RegistrationInfo": {
            "RegisterUri": "http://theme-test.learningsourceapp.com/register?sgid=5a8117d7cdf04676bb5217cf809211e6",
            "RegisterMessage": "Register"
        },
        "Name": "Developing Strengths into Talents",
        "Summary": "Most strategic planning processes work from today forward not from the future back - implicitly assuming, whatever the evidence to the contrary, that the future will be more or less like the present.",
        "StartDateTime": "2015-10-09T08:00:00.0000000+13:00",
        "EndDateTime": "2015-10-09T12:00:00.0000000+13:00",
        "TimeZone": "NZDT"
    }, {
        "ViewUri": "http://theme-test.learningsourceapp.com/courses/30-event-planning-and-management",
        "RegistrationInfo": {
            "RegisterUri": "http://theme-test.learningsourceapp.com/waiting-list?sid=781b8482f5d74c51bbd6d908b6580540",
            "RegisterMessage": "Join waiting list"
        },
        "Name": "Event Planning and Management",
        "Summary": "The AUMI Certificate in Management is recommended as a qualification suitable for people who are newly appointed into a management role, who are planning to move into a management role, or are looking to get an introductory management qualification.",
        "StartDateTime": "2015-10-09T08:00:00.0000000+13:00",
        "EndDateTime": "2015-10-09T12:00:00.0000000+13:00",
        "TimeZone": "NZDT"
    }, {
        "ViewUri": "http://theme-test.learningsourceapp.com/courses/30-event-planning-and-management",
        "RegistrationInfo": {
            "RegisterUri": "http://theme-test.learningsourceapp.com/register?sgid=5047f926323a416ca4b7386dcf3a8ca4",
            "RegisterMessage": "Register"
        },
        "Name": "Event Planning and Management",
        "Summary": "The AUMI Certificate in Management is recommended as a qualification suitable for people who are newly appointed into a management role, who are planning to move into a management role, or are looking to get an introductory management qualification.",
        "StartDateTime": "2015-10-09T08:00:00.0000000+13:00",
        "EndDateTime": "2015-10-09T12:00:00.0000000+13:00",
        "TimeZone": "NZDT"
    }, {
        "ViewUri": "http://theme-test.learningsourceapp.com/courses/50-health-and-safety-office",
        "RegistrationInfo": {
            "RegisterUri": "http://theme-test.learningsourceapp.com/register?sgid=cbfc9795284748dc9927cbcd2ec48048",
            "RegisterMessage": "Register"
        },
        "Name": "Health and Safety Office",
        "Summary": "On successful completion, graduates will be equipped to manage projects of challenging scale, complexity and uncertainty in a range of contexts to the satisfaction of demanding clients and other stakeholders.",
        "StartDateTime": "2015-10-09T08:00:00.0000000+13:00",
        "EndDateTime": "2015-10-09T16:30:00.0000000+13:00",
        "TimeZone": "NZDT"
    }, {
        "ViewUri": "http://theme-test.learningsourceapp.com/courses/50-health-and-safety-office",
        "RegistrationInfo": {
            "RegisterUri": "http://theme-test.learningsourceapp.com/register?sgid=b496525566bc46a3980ac369714c1137",
            "RegisterMessage": "Register"
        },
        "Name": "Health and Safety Office",
        "Summary": "On successful completion, graduates will be equipped to manage projects of challenging scale, complexity and uncertainty in a range of contexts to the satisfaction of demanding clients and other stakeholders.",
        "StartDateTime": "2015-10-09T08:00:00.0000000+13:00",
        "EndDateTime": "2015-10-09T16:30:00.0000000+13:00",
        "TimeZone": "NZDT"
    }]
}