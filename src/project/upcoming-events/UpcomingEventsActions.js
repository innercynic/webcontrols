var constants = require('./constants');
var dispatch = require('project/shared/helpers/dispatch');
var Backbone = require('backbone');
var _ = require('underscore');

function getFilterQueryString(filter){
    var filters= [];

    if(_.size(filter) > 0){
        _.map(filter, function (value, key){
            filters.push(key + "=" + value);
        });
    }

    return "&filter=" + filters.join(",");
}

function getFacetsQueryString(facets){
    return "&facets=" + facets.join(",");
}


module.exports = {
    find: function(filter) {
        dispatch(constants.UPCOMINGEVENTS_RETRIEVE, { filter: filter });

        //Could be passed in as config
        var facets = ['templatecategory', 'city', 'presenter'];

        var url = "https://api.learningsourceapp.com/theme-test/api/2012-02-01/pub/resources/eventsearch/"
            + "?format=json"
            + "&fields=EventID,Name,StartDateTime,EndDateTime,ViewUri,Summary,TimeZone,Location,RegistrationInfo,TemplateCode"
            + getFacetsQueryString(facets)
            + getFilterQueryString(filter);

        Backbone.$.getJSON(url, {format: 'json'}, 
            function(results) {
                if(results && results.Items) {
                    dispatch(constants.UPCOMINGEVENTS_RETRIEVE_SUCCESS, { items: results.Items, facets: results.Facets });
                } else {
                    dispatch(constants.UPCOMINGEVENTS_RETRIEVE_FAIL);
                }
            });
    }
};
