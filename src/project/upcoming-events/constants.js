module.exports = {
    UPCOMINGEVENTS_RETRIEVE: 'UPCOMINGEVENTS_RETRIEVE',
    UPCOMINGEVENTS_RETRIEVE_SUCCESS: 'UPCOMINGEVENTS_RETRIEVE_SUCCESS',
    UPCOMINGEVENTS_RETRIEVE_FAIL: 'UPCOMINGEVENTS_RETRIEVE_FAIL'
};
