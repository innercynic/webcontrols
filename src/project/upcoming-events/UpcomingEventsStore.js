var Backbone = require('backbone');
var Store = require('project/shared/libs/Store');
var constants = require('./constants');


var UpcomingEventsResult = Backbone.Model.extend({
    // getFacets: function() {
    //     console.log("facets...");
    //     console.log(this.facets);
    //     console.log(this.get('facets'));
    //     return this.get('facets');
    // }
});


class UpcomingEventsCollection extends Store.Collection {
    constructor() {
        this.model = UpcomingEventsResult;
        super();
    }

    initialize() {
        super.initialize();

    }

    handleDispatch(payload) {
        switch (payload.actionType) {
            case constants.UPCOMINGEVENTS_RETRIEVE_SUCCESS:
                this.reset();
                this.add(payload.items);
                break;
        }
    }
}

module.exports = new UpcomingEventsCollection();
