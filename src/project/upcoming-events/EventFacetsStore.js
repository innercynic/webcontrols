var Backbone = require('backbone');
var Store = require('project/shared/libs/Store');
var constants = require('./constants');


var EventFacet = Backbone.Model.extend({
    // getFacets: function() {
    //     console.log("facets...");
    //     console.log(this.facets);
    //     console.log(this.get('facets'));
    //     return this.get('facets');
    // }
});


class EventFacetsCollection extends Store.Collection {
    constructor() {
        this.model = EventFacet;
        super();
    }

    initialize() {
        super.initialize();
        
    }

    handleDispatch(payload) {
        switch (payload.actionType) {
            case constants.UPCOMINGEVENTS_RETRIEVE_SUCCESS:
                this.reset();
                this.add(payload.facets);
                break;
        }
    }
}

module.exports = new EventFacetsCollection();
