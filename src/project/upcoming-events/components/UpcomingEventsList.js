var React = require('react');
var moment = require('moment');


module.exports = React.createClass({
    getDate: function(date){
        return moment(date).format("dddd, DD MMM YYYY");
    },
    getTime: function(start, end, timezone){
        var startTime = moment(start).format("HH:mm A");
        var endTime = moment(end).format("HH:mm A");
        return startTime + " - " + endTime + " " + timezone;
    },
    render: function() {
        if(this.props.UpcomingEventsStore.length === 0) {
            return <p>No results.</p>
        }

        return <div className='row'>
            {this.props.UpcomingEventsStore.models.map((result)=> 
                
                <div className='col-xs-12 col-sm-6 col-md-3 upcoming-event-container' key={result.attributes.EventID}>
                  <div className="upcoming-event">
                    <h4><a href={result.attributes.ViewUri}>{result.attributes.Name}</a></h4>
                    <h5><strong>{this.getDate(result.attributes.StartDateTime)}</strong></h5>
                    <h5>{this.getTime(result.attributes.StartDateTime, result.attributes.EndDateTime, result.attributes.TimeZone)}</h5>
                    <h5>{result.attributes.Location.Name}</h5>
                    <div className="event-info">
                        <p>{result.attributes.Summary}</p><div className="read-more">...<a>read more</a></div>
                    </div>
                    <a href={result.attributes.RegistrationInfo.RegisterUri} className="btn btn-info" type="submit">{result.attributes.RegistrationInfo.RegisterMessage}</a>
                    </div>
                   </div>
               )}
            </div>
        }
    });