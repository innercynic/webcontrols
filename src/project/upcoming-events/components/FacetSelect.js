var React = require('react');
var pluralize = require('pluralize');

module.exports = React.createClass({
     getInitialState: function() {
         return {
             value: 'select'
         }
     },
     change: function(event){
        this.props.selectChange(event.target);
     },
     getPlural: function(singlular){
      return pluralize.plural(singlular);
     },
     render: function(){       
        return(
               <select id={this.props.id} className="form-control" onChange={this.change} value={this.props.filterState}>
                   <option value={-1}>All {this.getPlural(this.props.name)}</option>
                   {this.props.values.map((facet)=> 
                    <option value={facet.Code}>{facet.Label} - {facet.Count}</option>
                   )}
               </select>
        );
     }
});