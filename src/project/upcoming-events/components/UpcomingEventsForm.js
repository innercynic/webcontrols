var React = require('react');
var FacetSelect = require('project/upcoming-events/components/FacetSelect');


module.exports = React.createClass({
	getFilterState: function(filterCode){
		if(this.props.filterStates[filterCode]){
			return this.props.filterStates[filterCode];
		} 
	},
    render: function() {
        if(!this.props.EventFacetsStore || this.props.EventFacetsStore.models.length === 0) {
            console.log("No results");
            return <p>Warning: No facet data found...</p>
        }

        return (
			<div className="form-inline">
		        {this.props.EventFacetsStore.models.map((facet)=> 
					<div className="form-group">
			        	<label htmlFor={facet.attributes.FilterCode}>{facet.attributes.Name}</label>
				        <FacetSelect selectChange={this.props.selectChange} 
				        filterState={this.getFilterState(facet.attributes.FilterCode)} name={facet.attributes.Name}
				        values={facet.attributes.Values} id={facet.attributes.FilterCode} />
			        </div>
			    )}
			</div>
        );
    }
});