var React = require('react');
var storeMixin = require('project/shared/helpers/storeMixin');

var UpcomingEventsActions = require('../UpcomingEventsActions');
var UpcomingEventsStore = require('../UpcomingEventsStore');
var EventFacetsStore = require('../EventFacetsStore');

 var UpcomingEventsForm = require('./UpcomingEventsForm');
var UpcomingEventsList = require('./UpcomingEventsList');


module.exports = React.createClass({
    mixins: [storeMixin(UpcomingEventsStore), storeMixin(EventFacetsStore)],

    getInitialState: function() {
        return {
            UpcomingEventsStore: UpcomingEventsStore,
            EventFacetsStore: EventFacetsStore,
            filterStates: {}
        };
    },

    componentDidMount: function() {
        //if(this.props.routeParams && this.props.routeParams[0]) {
            UpcomingEventsActions.find();
        //}
    },

    componentWillReceiveProps: function(newProps) {
        //if(newProps.routeParams && newProps.routeParams[0]) {
            //UpcomingEventsActions.find();
        //}
    },

    onSearch: function(query) {
        RouterActions.navigate("UpcomingEvents/"+ encodeURIComponent(query));
    },

    onSelectChange: function(element){
        if(element.value != -1){
            this.state.filterStates[element.id] = element.value;
        } else{
            delete this.state.filterStates[element.id];
        }
        
        UpcomingEventsActions.find(this.state.filterStates);
    },


    render: function() {
        return <div className="container">
                    <div className="row">
                        <UpcomingEventsForm filterStates={this.state.filterStates} selectChange={this.onSelectChange} EventFacetsStore={this.state.EventFacetsStore}/> 
                    </div>
                    <UpcomingEventsList UpcomingEventsStore={this.state.UpcomingEventsStore} />
               </div>
    }
});