require('./shared/polyfills/Object.assign');

var React = require('react');
var App = require('./app/components/App');

require('./main.scss');


React.renderComponent(<App />, document.getElementById('app'));
