var React = require('react');

var UpcomingEvents = require('project/upcoming-events/components/UpcomingEvents');


module.exports = React.createClass({
    render: function() {
        return <div>
            <UpcomingEvents />
        </div>;
    }
});
